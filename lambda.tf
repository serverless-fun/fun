resource "aws_lambda_function" "lambda_fun" {
  function_name = "lambda_fun_${terraform.workspace}"

  s3_bucket = var.lambda_bucket
  s3_key    = "${var.lambda_version}/fun.zip"

  runtime = "python3.8"
  handler = "handler.handler"

  source_code_hash = base64sha256(file("fun/handler.py"))

  role = aws_iam_role.lambda_exec.arn

  environment {
    variables = {
      LAMBDA_ENVIRONMENT = "${terraform.workspace}"
    }
  }
}

# IAM role for the Lambda function
resource "aws_iam_role" "lambda_exec" {
  name = "lambda_fun_${terraform.workspace}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}


