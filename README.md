# Lambda Fun!

This repository contains AWS Infrastructure as Code and CI/CD defintions to deploy Lambda
functions in a close-to-production fashion.

## What is in this repo?

![architecture](./docs/images/architecture.png "Architecture")

In the diagram above you can see the intended components to be built by this repository.
Only the parts in dashed red line are currently included. These are AWS API Gateway
definitions, a sample Lambda Function.

The deployment is done using [Terraform](https://www.terraform.io/) and driven by
[GitLabCI](https://docs.gitlab.com/ee/ci/).


### Repository stucture


```shell
├── docs
│   └── images
├── fun
│   ├── handler.py
│   └── test_handler.py
├── api_gateway.tf
├── lambda.tf
├── outputs.tf
├── provider.tf
├── README.md
└── variables.tf
```

The `fun` directory contains the lambda function defintion. The `docs` directory contains
assets to make this `README.md` as pretty as it is :smile:. The remaining `.tf` files
should be self explanatory by their names. They could be collected in a module with a bit
more time to increase reusability and code organization.

## Lambda Function

The lambda function is very simple, it simply returns an AWS API Gateway compliant JSON
value similar to this one:

```json
{
  "statusCode": 200,
  "headers": {"Content-Type": "text/html; charset=utf-8"},
  "body": "hello, something!"
}
```

The message body is dependent on the environment variable `LAMBDA_ENVIRONMENT` to spice
things up a bit and demonstrate the use of several environments.

### Testing

A simple test suite is included and can be run with [pytest](https://docs.pytest.org/en/6.2.x/):

```
pytest fun
```

## AWS Infrastructure

The AWS infrastructure is simply the Lambda function and the API Gateway components with
the additional IAM roles necessary to wire them together. The net result looks like this:

[apigw](./docs/images/apigw.png "API GateWay")

## CI/CD

CI/CD is kept simple. The main idea is to deploy an entire copy of the stack using
isolated from eachother using [Terraform
Workspaces](https://www.terraform.io/docs/language/state/workspaces.html). The default
workspace is reserved for the main branch.

The idea is to give every developer a separate copy to work and test on and ultimately
merge their changes to main. Once this is done their changes are live on the main URL!

This flow is summarized in the diagram with a funny sticky man below:

![cicd](./docs/images/cicd.png "Architecture")

Ideally, after a merge request an existing workspace should be deleted, but this
functionality is currently not implemented in this repository.

A positive side effect of this flow is that functions are also kept in a S3 bucket, so
can be easily redeployed by forcing the `LAMBDA_VERSION` variable, but this functionality is
currently not implemented in this repository.

A positive side effect of this flow is that functions are also kept in a S3 bucket, so
can be easily redeployed by forcing the `LAMBDA_VERSION` variable. Some work might still
need to be done to align the URL with the version.

Another important part would be to wire existing production DNS records with API Gateway
Endpoints.

### My company needs DTAP!

If that's the case, with some more time I would decouple the most sensitive deployment
repository (production) from this "development" one. You can still deploy to a common
Acceptance environment using this flow.

Using S3 as an intermediate storage for lambda function packages allow to decouple the
build and the deployment phases in a relatively natural way. The packages can then be
reused in other repositories.

To protect production and increase auditability I would use GitLab Groups to restrict
access to it and allow modifications only through Merge Requests with mandatory review
rules.

The Production CI/CD could be triggered either manually by explicitly updating the
version of a newly created lambda function package or automatically using webhooks or
commits.

Finally, the production infrastructure might differ from the development one, so it would
also add in flexibility.

### Ok, but does it work?

Yes! Here are some screenshots of the main branch:

![main](./docs/images/hello_default.png "Main")

and a feature branch:

![feature](./docs/images/hello_commit.png "Feature")

But you can check for yourself:

* [main branch](https://17eshc30pi.execute-api.eu-west-1.amazonaws.com/fun)
* [feature branch](https://srwd8g6x92.execute-api.eu-west-1.amazonaws.com/fun)

## Next steps

Observability is an important part that is not included in this repository. The main
component to consider are: Logging, Metrics, Alerting. Additionally, the tooling selected
would allow the creation of Dashboards.

### Logging

Cloud Watch Logging is enabled by default for [Lambda
Functions](https://docs.aws.amazon.com/lambda/latest/dg/python-logging.html) can be setup
for [API
Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-logging.html).

Log events can then be [streamed from CloudWatch to an Elasticsearch
instance](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CWL_ES_Stream.html).
This would give the ability to developers to make more complex queries, for example
request ids and correlate messages across the entire system. Another important advantage
would be building Dashboards using
[Kibana](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CWL_ES_Stream.html)
which is included in AWS Elasticsearch service.

### Metrics

While logs are important to understand the behaviour of the internal of the functions,
another type of information is constituted by functional metrics, such as number of
lambda invocations and performance related information. An overview can be found
[here](https://docs.aws.amazon.com/lambda/latest/dg/monitoring-metrics.html). AWS
CloudWatch offer specific tools for Lambda functions such as [Lambda
Insights](https://docs.aws.amazon.com/lambda/latest/dg/monitoring-insights.html).

### Alerts

Both [CloudWatch
Alarms](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/AlarmThatSendsEmail.html)
and [Elasticsearch
Alerts](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/alerting.html)
can be configured to deliver notifications to existing incident response platform such as
[PagerDuty](https://www.pagerduty.com/pricing/). Conditions that trigger an alert can be
expressed using specific CloudWatch or Elasticsearch expressions.

### Dashboards

Additional operational dashboards can be built using
[Grafana](https://aws.amazon.com/grafana/) (the managed AWS service is still in preview
in most regions) or the already mentioned Kibana.

## How did I put this together?

I made a small plan and followed the steps in this [TODO.md](./TODO.md) document!
