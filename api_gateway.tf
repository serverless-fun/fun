# API GW
resource "aws_api_gateway_rest_api" "lambda_fun" {
  name        = "lambda_fun_${terraform.workspace}"
  description = "lambda_fun makes Lambda Fun!"
}

# proxy all the paths!
resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.lambda_fun.id
  parent_id   = aws_api_gateway_rest_api.lambda_fun.root_resource_id
  path_part   = "{proxy+}"
}

# proxy all the HTTP methods!
resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = aws_api_gateway_rest_api.lambda_fun.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "ANY"
  authorization = "NONE"
}

# add lambda invocation
resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = aws_api_gateway_rest_api.lambda_fun.id
  resource_id = aws_api_gateway_method.proxy.resource_id
  http_method = aws_api_gateway_method.proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_fun.invoke_arn
}

# add proxy for root path
resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = aws_api_gateway_rest_api.lambda_fun.id
  resource_id   = aws_api_gateway_rest_api.lambda_fun.root_resource_id
  http_method   = "ANY"
  authorization = "NONE"
}

# add invocation for root path
resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = aws_api_gateway_rest_api.lambda_fun.id
  resource_id = aws_api_gateway_method.proxy_root.resource_id
  http_method = aws_api_gateway_method.proxy_root.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_fun.invoke_arn
}

# deployment
resource "aws_api_gateway_deployment" "lambda_fun" {
  depends_on = [
    aws_api_gateway_integration.lambda,
    aws_api_gateway_integration.lambda_root,
  ]

  rest_api_id = aws_api_gateway_rest_api.lambda_fun.id
  stage_name  = "fun"
}

# allow API GW to invoke lambda
resource "aws_lambda_permission" "apigw" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_fun.function_name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.lambda_fun.execution_arn}/*/*"
}
