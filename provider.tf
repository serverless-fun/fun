terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  backend "s3" {
    bucket = "riccardomc-lambda-fun-terraform"
    key    = "state"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = "eu-west-1"
}
