# TODO

This is simply the plan I made for myself to populate this repository. It took me about
3-4 hours of work in 3 separate sessions. :)

Create Sample Lambda Function
-----------------------------
1. Create handler
2. return "hello world!" text
3. Add functionality to read ENV_VAR
4. return "hello, $ENV_VAR"! text
5. create GitLab CI to run tests

Deploy with Terraform
---------------------
1. create IAM role
2. Introduce AWS provider
3. Introduce S3 backend
4. Add terraform code to deploy lambda function
5. deploy the function from your local machine
6. add credentials in GitLab CI
7. deploy the function using Gitlab CI

Add API GW
----------
1. Introduce API GW
2. Wire lambda endpoint
3. Enable stages in API GW

Enhance CICD Workflow
---------------------
1. Create a new lambda/APIGW version for each merge request
2. Update main version upon merging

Add DNS
-------
1. Create Route53 zone
2. Add DNS record to point to API GW

Make plan for monitoring
------------------------
1. Mention Elasticsearch
2. CloudWatch alarms

Write Documentation
-------------------
1. Create Diagram

Resources
---------
* https://learn.hashicorp.com/tutorials/terraform/lambda-api-gateway
* https://docs.gitlab.com/ee/ci/environments/#create-a-dynamic-environment
* https://www.terraform.io/docs/language/settings/backends/s3.html
* https://www.terraform.io/docs/cloud/guides/recommended-practices/part1.html#one-workspace-per-environment-per-terraform-configuration
