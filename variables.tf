variable "lambda_version" {
  type        = string
  description = "The version of the lambda function to use"
}

variable "lambda_bucket" {
  type        = string
  description = "The bucket where the lambda function code resides"
}
