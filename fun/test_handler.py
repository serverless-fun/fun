import pytest
import os

from handler import handler


@pytest.mark.parametrize(
    "environment,expected_value",
    [
        (
            "dev",
            {
                "statusCode": 200,
                "headers": {"Content-Type": "text/html; charset=utf-8"},
                "body": "hello, dev!",
            },
        ),
        (
            "jimmy",
            {
                "statusCode": 200,
                "headers": {"Content-Type": "text/html; charset=utf-8"},
                "body": "hello, jimmy!",
            },
        ),
        (
            "",
            {
                "statusCode": 200,
                "headers": {"Content-Type": "text/html; charset=utf-8"},
                "body": "hello, world!",
            },
        ),
        (
            None,
            {
                "statusCode": 200,
                "headers": {"Content-Type": "text/html; charset=utf-8"},
                "body": "hello, world!",
            },
        ),
    ],
)
def test_handler(environment, expected_value):
    # given
    if os.environ.get("LAMBDA_ENVIRONMENT"):
        del os.environ["LAMBDA_ENVIRONMENT"]
    if environment:
        os.environ["LAMBDA_ENVIRONMENT"] = environment

    # when
    actual_value = handler(None, None)

    # then
    assert actual_value == expected_value
