import os


def handler(event, context):
    """
    Environment based greetings, AWS Lambda handler function.

    https://docs.aws.amazon.com/lambda/latest/dg/python-handler.html

    :param event: the event, we don't actually use it
    :param context: the context, same...
    :returns: a JSON formatted string with a message
    """

    environment = os.environ.get("LAMBDA_ENVIRONMENT", "")
    if not environment or len(environment) == 0:
        message = "hello, world!"
    else:
        message = "hello, %s!" % environment
    return_value = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "text/html; charset=utf-8",
        },
        "body": message,
    }

    return return_value
